Here I will complete Tuesday's tasks: install nginx remotely on the new vm

The bash script has the commands so that when I run it on the virtual machine, nginx will be installed and running.

To send the bash script to ubuntu, I need to make sure I'm not logged into ubuntu but on my personal user. I will use:

```bash
$ scp -i ~/.ssh/ch9_shared.pem -r bash_script_for_vm ubuntu@54.229.70.63:/home/ubuntu/bash_for_nginx
```
This has sent my bash script to a folder that I made while logged into ubuntu called bash_for_nginx.

Now from ubuntu, I will cd into the dolfer where the bash script is and use the commands:

```
$ chmod +x ./bash_script_for_vm
$ ./bash_script_for_vm
```

in order to run the file.